import styled from 'styled-components';

export const SearchInput = styled.input`
    padding: 12px 8% 12px 2%;
    line-height: 15px
`;
