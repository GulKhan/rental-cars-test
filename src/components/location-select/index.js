import React from 'react'
import PropTypes from 'prop-types'
import SearchSelect from '../search-select';
import {
   SelectOption,
   OptionTypeLabel,
   OptionNameLabel,
   Link,
   Error
} from '../search-select/styles.js';

/**
 * Component which displays a location selection input field.
 */
class LocationSelect extends React.Component {

   constructor(props) {
      super(props);
      this.state = {
         // dummy data commented out, was used when the endpoint reached the limit of allowed requests
         options: [],//[{"country":"United Kingdom","lng":-2.27472,"city":"Manchester","searchType":"L","alternative":["GB,UK,England,Manchester Airport"],"bookingId":"airport-38566","placeType":"A","placeKey":"1472187","iata":"MAN","countryIso":"gb","locationId":"38566","name":"Manchester Airport","ufi":900038550,"region":"Greater Manchester","lang":"en","lat":53.3536},{"country":"United Kingdom","lng":-2.23615,"searchType":"L","alternative":["GB,UK,England"],"bookingId":"city-2623580","placeType":"C","placeKey":"441725","countryIso":"gb","locationId":"20951","name":"Manchester","ufi":-2602512,"region":"Greater Manchester","lang":"en","lat":53.4812},{"country":"United States of America","lng":-73.9805,"searchType":"L","alternative":["US,US,USA,NY,New York Metropolitan area"],"bookingId":"district-929","placeType":"D","placeKey":"1471134","countryIso":"us","locationId":"-1","name":"Manhattan","ufi":20088325,"region":"New York State","lang":"en","lat":40.7573},{"country":"Nicaragua","lng":-86.1681,"city":"Managua","searchType":"L","alternative":["NI,Augusto C. Sandino"],"bookingId":"airport-26411","placeType":"A","placeKey":"1472509","iata":"MGA","countryIso":"ni","locationId":"26411","name":"Managua Airport","ufi":900038755,"lang":"en","lat":12.1411},{"country":"Nicaragua","lng":-86.2683,"searchType":"L","alternative":["NI"],"bookingId":"city-1798053","placeType":"C","placeKey":"886503","countryIso":"ni","locationId":"15416","name":"Managua","ufi":-1113265,"lang":"en","lat":12.1508},{"lng":-4.63451,"city":"Derbyhaven","searchType":"L","alternative":["IM"],"bookingId":"airport-19471","placeType":"A","placeKey":"1472264","iata":"IOM","countryIso":"im","locationId":"19471","name":"Ronaldsway Airport","ufi":900038781,"region":"Isle of Man","lang":"en","lat":54.0868}],
         error: ''
      }
   }

   /**
    * Get the selection data from the url endpoint.
    * This must return a promise
    *
    * @param url
    * @returns {Promise.<TResult>}
    */
   fetchData(url) {
      try {
         return fetch(url)
            .then(res => {
               if (!res.ok) {
                  this.setState({
                     error: res.statusText
                  });
                  return res;
               } else {
                  return res.json()
               }
            })
            .then(res => {
               this.setState({
                  options: res.results.docs
               });
            })
            .catch((e) => {
               return Promise.resolve(() => {
                  this.setState({
                     error: e,
                     options: []
                  });
               })
            })
      } catch (e) {
         this.setState({
            error: "There was a problem fetching the results from the endpoint"
         });
      }
   }


   /**
    * Render the select menu options using the data returned from the endpoint
    * @param searchPhrase
    * @returns {Array}
    */
   renderOptions(searchPhrase) {

      return this.state.options.map(option => {
         let type = option.bookingId.split('-')[0];
         let splitName = searchPhrase && option.name.indexOf(searchPhrase) !== -1 ? option.name.split(searchPhrase) : option.name;

         let name = option.name+(option.hasOwnProperty('iata') ? ' ('+option.iata+')' : '');
         let displayName = name;
         let location = [];
         if (option.hasOwnProperty('city')) {
            displayName += ', '+option.city;
            location.push(option.city);
         }
         if (option.hasOwnProperty('country')) {
            displayName += ', '+option.country;
            location.push(option.city);
         }
         location = location.join(', ');

         return (
            <li key={option.locationId} className={'item'} onClick={() => this.props.onSelect(option.locationId, displayName)}>
               <Link>
                  <OptionTypeLabel className={type}>{type}</OptionTypeLabel>
                  <OptionNameLabel className={'text'}>
                     {splitName.constructor === Array &&
                        <span>
                           {splitName[0]}<em style={{fontWeight: 'bold'}}>{searchPhrase}</em>{splitName[1]}
                        </span>
                     }
                     {splitName.constructor !== Array && name}
                     <br/>
                     {location}
                  </OptionNameLabel>
               </Link>
            </li>
         )
      })


   }

   render() {
      return (
         <SearchSelect
            id={this.props.id}
            value={this.props.value}
            placeholderText={this.props.placeholderText}
            url={"https://cors.io/?https://www.rentalcars.com/FTSAutocomplete.do?solrIndex=fts_en&solrRows=6&solrTerm={search_term}"}
            renderOptions={(searchPhrase) => this.renderOptions(searchPhrase)}
            fetchData={(url) => this.fetchData(url)}
            options={this.state.options}
         >
            {this.state.error &&
               <Error>{this.state.error}</Error>
            }
         </SearchSelect>
      )
   }
}

LocationSelect.defaultProps = {
   placeholderText: 'city, airport, station, region and district...',
   isFocused: false,
   value: null
};

LocationSelect.propTypes = {
   id: PropTypes.string.isRequired,
   value: PropTypes.shape({
      id: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
   }).isRequired,
   placeholderText: PropTypes.string,
   onSelect: PropTypes.func.isRequired
}

export default LocationSelect