const { fromJS } = require('immutable')

export const SET_PU_LOCATION_ID = 'search-box/SET_PU_LOCATION_ID'

const initialState = fromJS({
   puLocationID: '',
   puLocationLabel: '',
})

export default (state = initialState, action) => {
   switch (action.type) {
      case SET_PU_LOCATION_ID:
         state = state.set('puLocationID', action.id);
         return state.set('puLocationLabel', action.label);
      default:
         return state
   }
}

export const setPULocation = (id, label) => {
   return dispatch => {
      dispatch({
         type: SET_PU_LOCATION_ID,
         id,
         label
      })
   }
}

